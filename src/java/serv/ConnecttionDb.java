/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import java.sql.Connection;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * ПОДКЛЮЧЕНИЕ К БД
 * @author alesha
 */
public class ConnecttionDb {
    public static Connection connect(){
    Connection con = null;
    try {
          InitialContext ctx =  new InitialContext();
          DataSource ds = (DataSource) ctx.lookup("java:/Ulog");
          con =  ds.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    return con;
    
    }
}
