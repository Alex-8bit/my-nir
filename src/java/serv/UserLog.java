/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import java.util.Date;
//import java.sql.Date;

/**
 *
 * @author alesha
 */
public class UserLog {
    private int seansid;
    private int userid;
    private Date tstamp;
    private   int count;
    private   int hour;
    private String date;

    public int getSeansid() {
        return seansid;
    }

    public void setSeansid(int seansid) {
        this.seansid = seansid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public Date getTstamp() {
        return tstamp;
    }

    public void setTstamp(Date tstamp) {
        this.tstamp = tstamp;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    

}
