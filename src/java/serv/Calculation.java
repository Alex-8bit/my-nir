/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *
 * @author alesha
 */
public class Calculation {
    
    static Logger Log = Logger.getLogger(DbWorker.class.getName());
    
    public static void test (){
        ArrayList<WorkLog> test = new ArrayList<>();
        test.addAll(DbWorker.workloglist());
        for (WorkLog w: test){
            Log.info("datasize long = " + w.getDatasize() + "; datasize MB = " + w.getDatasizeMB());
        }
    }
    public static String convertToMegaBytes(long datasize){
        String result = "";
        try {
            Log.info("datasize = " + datasize);
            int megabytes = (int) (datasize/1024/1024);
            if (megabytes == 0){
                int kilobytes = (int) (datasize/1024);
                if (kilobytes == 0){
                    result = String.valueOf(datasize).concat(" bytes");
                }else {
                    result = String.valueOf(kilobytes).concat(" kilobytes");
                }
            } else{
                result = String.valueOf(megabytes).concat(" megabytes");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
