/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;
//package org.primefaces.showcase.view.input;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.chart.LineChartModel;
import static serv.DbWorker.Log;

/**
 *
 * @author alesha
 */
@ManagedBean(name = "calendarView")
@ViewScoped
public class CalendarView implements Serializable {

    static Logger Log = Logger.getLogger(CalendarView.class.getName());
    private Date monthly1;
    private Date monthly2;
    public static Date date1ForPeriod;
    public static Date date2ForPeriod;
    private int selRadio = 1;

    public static Date dailystat;
    private ArrayList<UserLog> userLogListForOneDay;
    public static ArrayList<UserLog> userLogListForPeriod = new ArrayList<>();
    public static ArrayList<UserLog> userLogListForOneDayfor24hours = new ArrayList<>();
    public static int countSeansesForOneDate;
    private HashMap<java.util.Date, Integer> dataCount = new HashMap<>();

    public static void showMessage() {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Ошибка заполнения", "Заполните поле");
        PrimeFaces.current().dialog().showMessageDynamic(message);
    }

    /**
     * Функция считает кол-во сеансов для заданной даты для каждого часа
     *
     * @return список объектов UserLog с заполненными полями hours и count
     */
    public ArrayList<UserLog> countSeansForDayByHours() {
        ArrayList<UserLog> resultList = new ArrayList<>(); // Создаем пустой массив и инициализируем его
        UserLog u;// объявили объект класса Userlog
        java.util.Date date1 = new Date(); // Создаем переменную date1 
        date1 = dailystat; // переменной date1 приравниваем dailystat (Выбранную дату с веб страницы формат ГГ-ММ-ДД)
        if (dailystat == null) {
            CalendarView.showMessage();
        }
        userLogListForOneDayfor24hours = new ArrayList<>();
        Calendar cal1 = Calendar.getInstance();//Календарь задаем ему значение по умолчанию (так нужно)
        Calendar cal2 = Calendar.getInstance();//Календарь задаем ему значение по умолчанию (так нужно)
        cal1.setTime(date1);// 
        cal2.setTime(date1);//
        Log.info("date1 = " + date1);
        Log.info("cal1 = " + cal1);
        Log.info("date1 = " + date1);
        for (int i = 0; i < 24; i++) { // пробегаем по массиву данных  
            int res = 0;
            Log.info("i = " + i);
            cal2.add(Calendar.HOUR, 1); // прибавляем +1 час 
            u = new UserLog();
            Timestamp tst1 = new Timestamp(cal1.getTime().getTime()); // инициализируем переменные типа timestamp и приводит выбранную дату к виду ГГ-ММ-ДД ЧЧ-ММ-СС.МС

            Timestamp tst2 = new Timestamp(cal2.getTime().getTime()); // инициализируем переменные типа timestamp и приводит выбранную дату к виду ГГ-ММ-ДД ЧЧ-ММ-СС.МС

            Log.info("tst1 = " + tst1 + "; tst2 = " + tst2);
            try {
                res = DbWorker.countSeansesBetweenTwoTimestamps(tst1, tst2);// присваиваем res результат функции из класса DbWorker 
            } catch (SQLException ex) {
                Logger.getLogger(CalendarView.class.getName()).log(Level.SEVERE, null, ex);
            }
            u.setHour(i);
            u.setCount(res);
            Log.info("res = " + res);
            resultList.add(u);
            cal1.setTime(cal2.getTime());
        }
        userLogListForOneDayfor24hours.addAll(resultList);
        System.out.println("resultList len = " + resultList.size());
        PrintGrafig pg = new PrintGrafig();
//                 pg.getAnimatedModel1();
        pg.createAnimatedModels();
//          pg.getAnimatedModel1();
//          pg.getLineModel();
//         
        return resultList;
    }

    /**
     * Функция считает кол-во сеансов для заданного периода
     *
     * @return список объектов UserLog с заполненными полями date и count
     */
    public ArrayList<UserLog> countSeansesForPeriod() {
        ArrayList<UserLog> resultList = new ArrayList<>(); // Создаем пустой массив и инициализируем его
        UserLog u;// объявили объект класса Userlog
        java.util.Date date1 = new Date(); // Создаем переменную date1 
        java.util.Date date2 = new Date(); // Создаем конечную дату datafinish
        date1 = date1ForPeriod; // переменной date1 присваиваем значение date1ForPeriod (Выбранную дату с веб страницы формат ГГ-ММ-ДД)
        date2 = date2ForPeriod; // переменной date1 присваиваем значение date1ForPeriod (Выбранную дату с веб страницы формат ГГ-ММ-ДД)
        userLogListForPeriod = new ArrayList<>();
        Calendar cal1 = Calendar.getInstance();//Календарь задаем ему значение по умолчанию (так нужно)
        Calendar cal2 = Calendar.getInstance();//Календарь задаем ему значение по умолчанию (так нужно)
        Calendar cal1help = Calendar.getInstance();//Календарь задаем ему значение по умолчанию (так нужно)
        Calendar calFinish = Calendar.getInstance();//Календарь задаем ему значение по умолчанию (так нужно)

//        Log.info("date1 = " + date1 );
//        cal2.add(Calendar.DAY_OF_YEAR, 1);        
//        Log.info("date2 = " + cal2.getTime() );
/*

         */
        if (date1 != null) {
            cal1.setTime(date1); // здесь мы запомнили начальную дату, не меняем ее 
            cal1help.setTime(date1);
            if (date2 != null) {
                calFinish.setTime(date2);
                calFinish.add(Calendar.DAY_OF_YEAR, 1); // задали дату на день позже последней
                cal2.setTime(date2);//

                while (!cal1help.equals(calFinish)) {
                    int res = 0;
                    u = new UserLog();
                    u.setDate(String.format("%tF", cal1help.getTime())); // дату нужно подогнать под формат 
                    Timestamp tst1 = new Timestamp(cal1help.getTime().getTime());
                    cal1help.add(Calendar.DAY_OF_YEAR, 1);
                    Timestamp tst2 = new Timestamp(cal1help.getTime().getTime());
                    Log.info("tst1 = " + tst1 + "; tst2 = " + tst2);
                    try {
                        res = DbWorker.countSeansesBetweenTwoTimestamps(tst1, tst2);// присваиваем res результат функции из класса DbWorker 
                        Log.info("res = " + res);
                    } catch (SQLException ex) {
                        Logger.getLogger(CalendarView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    u.setCount(res);
                    resultList.add(u);
                }
            }
        }

        userLogListForPeriod.addAll(resultList);
//        PrintGrafig pg = new PrintGrafig();
//        pg.createAnimateModelForPeriod();
        return resultList;
    }

//    public void onDateSelect(SelectEvent event) {
//         
//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Выбранная дата:", format.format(event.getObject())));
//    }
    /**
     * функция, которая возвращает количество строк в UserLog для заданной даты
     *
     * @return
     */
    public static int returnCountSeansesForOneDay(java.util.Date inputDate) {
        int res = 0; // Целочисленна переменная, которая равна нулю 
        try {
            Calendar cal1 = Calendar.getInstance(); //Календарь задаем ему значение по умолчанию (так нужно)
            cal1.setTime(inputDate); // Получаем к выбранной дате из параметров время???
            cal1.add(Calendar.DAY_OF_YEAR, 1); // Добавляем к выбранно дате + 1 день???
            //  Log.info("date1 = " + inputDate + "; date2 = " + cal1.getTime());
            res = DbWorker.countSeansesBetweenTwoTimestamps(new Timestamp(inputDate.getTime()), new Timestamp(cal1.getTime().getTime()));//задаем значение результирующей переменной, результат получаем из функции CountSeansesForOneDate(в нее передаем начальную дату и конечную)
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res; // возращаем результат
    }

    /**
     * Функция для получения списка userLogов для суточной статистики
     */
    public void obtainingDailyStat() {
        ArrayList<UserLog> resultLogList = new ArrayList<>();
        java.util.Date date1 = new Date();
        java.util.Date date2 = new Date();
        try {
            if (dailystat == null) {
                Log.info("dailystat is null");
            } else {
                userLogListForOneDay = new ArrayList<>();
                Log.info("dailystat = " + dailystat);
//                Timestamp ts = new Timestamp(dailystat.getTime());
//                Log.info("timestamp = " + ts);       
                // выцепить из u.getTstamp() 2017-05-17 08:43:49.257021 только дату
                // вытащенную дату сравнить с пришедшей из формы календаря
//                Calendar cal1 = Calendar.getInstance();
//                cal1.setTime(dailystat);
//                cal1.add(Calendar.DAY_OF_YEAR, 1);
//                date2 = cal1.getTime();       
//                cal2.setTime(dailystat);
//                cal2.add(Calendar.DAY_OF_YEAR, -1);
//                date2 = cal2.getTime();
//                date1 = dailystat;
//                Log.info("date1 = " + date1 + " date2 = " + date2);
//                countSeansesBetweenTwoTimestamps = DbWorker.countSeansesBetweenTwoTimestamps(new Timestamp(date1.getTime()), new Timestamp(date2.getTime()));
//                resultLogList.addAll(DbWorker.userloglist(date2, date1));
//                userLogListForOneDay.addAll(resultLogList);
                countSeansesForOneDate = CalendarView.returnCountSeansesForOneDay(dailystat);
            }
//                   Log.info("result list size = " + resultLogList.size());
            dataCount.clear();
            dataCount.put(date2, countSeansesForOneDate);

        } catch (Exception e) {
        }

//        FacesContext facesContext = FacesContext.getCurrentInstance();
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        Log.info("метод получения суточной статистики" +format.format());
    }

    //________________________________
    public Date getDailystat() {
        return dailystat;
    }

    public void setDailystat(Date dailystat) {
        this.dailystat = dailystat;
    }

    public Date getMonthly1() {
        return monthly1;
    }

    public void setMonthly1(Date monthly1) {
        this.monthly1 = monthly1;
    }

    public Date getMonthly2() {

        return monthly2;
    }

    public void setMonthly2(Date monthly2) {
        this.monthly2 = monthly2;
    }

    public ArrayList<UserLog> getUserLogListForOneDay() {
        return userLogListForOneDay;
    }

    public void setUserLogListForOneDay(ArrayList<UserLog> userLogListForOneDay) {
        this.userLogListForOneDay = userLogListForOneDay;
    }

    public int getCountSeansesForOneDate() {
        return countSeansesForOneDate;
    }

    public void setCountSeansesForOneDate(int countSeansesForOneDate) {
        this.countSeansesForOneDate = countSeansesForOneDate;
    }

    public HashMap<Date, Integer> getDataCount() {
        return dataCount;
    }

    public void setDataCount(HashMap<Date, Integer> dataCount) {
        this.dataCount = dataCount;
    }

    public ArrayList<UserLog> getUserLogListForOneDayfor24hours() {
        return userLogListForOneDayfor24hours;
    }

    public void setUserLogListForOneDayfor24hours(ArrayList<UserLog> userLogListForOneDayfor24hours) {
        this.userLogListForOneDayfor24hours = userLogListForOneDayfor24hours;
    }

    public Date getDate1ForPeriod() {
        return date1ForPeriod;
    }

    public void setDate1ForPeriod(Date date1ForPeriod) {
        this.date1ForPeriod = date1ForPeriod;
    }

    public Date getDate2ForPeriod() {
        return date2ForPeriod;
    }

    public void setDate2ForPeriod(Date date2ForPeriod) {
        this.date2ForPeriod = date2ForPeriod;
    }

    public ArrayList<UserLog> getUserLogListForPeriod() {
        return userLogListForPeriod;
    }

    public void setUserLogListForPeriod(ArrayList<UserLog> userLogListForPeriod) {
        this.userLogListForPeriod = userLogListForPeriod;
    }

    public int getSelRadio() {
        return selRadio;
    }

    public void setSelRadio(int selRadio) {
        this.selRadio = selRadio;
    }

}
