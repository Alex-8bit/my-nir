/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import com.ibm.wsdl.util.StringUtils;
import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartSeries;

@ManagedBean(name = "printGrafig")
@SessionScoped
/**
 * ОТОБРАЖЕНИЕ ГРАФИКОВ
 *
 * @author alesha
 */
public class PrintGrafig implements Serializable {

    private LineChartModel animatedModel1;
    private LineChartModel animatedModelForPeriod;
    
    static Logger Log = Logger.getLogger(PrintGrafig.class.getName());

//    @PostConstruct
//    public void init() {
//      createAnimateModelForPeriod();
//      createAnimateModelForPeriod();
//    }
//              createAnimateModelForPeriod();
//        lineModel = new LineChartModel();
//        LineChartSeries s = new LineChartSeries();
//        s.setLabel("Series 1");
//        ArrayList<UserLog> userLogList = new ArrayList<>();
//        if (!CalendarView.userLogListForOneDayfor24hours.isEmpty()){
//        userLogList.addAll(CalendarView.userLogListForOneDayfor24hours);
//        }
//        s.set(1, 2);
//        if (userLogList.size() > 0) {
//            System.out.println("series x: " + s.getXaxis());
//            for (int i = 0; i < userLogList.size(); i++) {
////                series1.set(i, userLogList.get(i).getCount());
//                s.set(i, 2);
//                System.out.println("x:" + userLogList.get(i).getHour());
//                System.out.println("y:" + userLogList.get(i).getCount());
//            }
//        }
//        lineModel.addSeries(s);
//        lineModel.setLegendPosition("e");
//        Axis xAxis = lineModel.getAxis(AxisType.X);
//        xAxis.setMin(0);
//        xAxis.setMax(30);
//        xAxis.setLabel("Hours");
//        Axis yAxis = lineModel.getAxis(AxisType.Y);
//        yAxis.setMin(0);
//        yAxis.setMax(30); 
//        yAxis.setLabel("Seances");
    

    public void createAnimatedModels() {
        System.out.println("in createAnimatedModels!");
        animatedModel1 = initLinearModel();
        if (animatedModel1 == null) {
            System.out.println("animatedModel null");
        }
        animatedModel1.setTitle("Суточная статистика " + String.format("%tF",CalendarView.dailystat));
        animatedModel1.setAnimate(true);
        animatedModel1.setLegendPosition("ne");
       Axis xAxis = animatedModel1.getAxis(AxisType.X);
         animatedModel1.getAxis(AxisType.X).setLabel("Часы");
         animatedModel1.getAxis(AxisType.X).setTickFormat("%d");
//         animatedModel1.getAxis(AxisType.X).setTickAngle(-25);
//         xAxis.setTickAngle(20);//поворот по иксу
        xAxis.setMin(0);
        xAxis.setMax(23);
        Axis yAxis = animatedModel1.getAxis(AxisType.Y);
//        animatedModel1.getAxis(AxisType.Y).setTickFormat("%d");
        
//        animatedModel1.getAxis(AxisType.Y).setTickFormat("%o");
        animatedModel1.getAxis(AxisType.Y).setLabel("Кол-во посещений");
         yAxis.setMin(0);
//         animatedModel1.setStacked(true);//большое разбиение
//        yAxis.setMax(5);       
    }

    public LineChartModel initLinearModel() {
        System.out.println("in initLinear");
        LineChartModel model = new LineChartModel();
        ArrayList<UserLog> userLogList = new ArrayList<>();
        userLogList.addAll(CalendarView.userLogListForOneDayfor24hours);
        LineChartSeries series1 = new LineChartSeries();
        
        series1.setLabel("Визиты");
        if(userLogList != null){
            if (userLogList.size() > 0) {
                try {
                    for (int i = 0; i < userLogList.size(); i++) {
                        series1.set(userLogList.get(i).getHour(), userLogList.get(i).getCount());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        
      
        
        
//        if(userLogList == null){
//            System.out.println("userLogList IS EMPTYYYYYYYYY");
//        }
//        else{
//            System.out.println("userlogSIZE" + userLogList.size());
//        }
////        if (!CalendarView.userLogListForOneDayfor24hours.isEmpty()){
//////        series1.set(1, 2);
////        series1.set(17, 2);
////        series1.set(20, 22);
////        series1.set(11,7);
//     
//        
////        for (UserLog u: CalendarView.userLogListForOneDayfor24hours){
////            series1.set(u.getHour(), u.getCount());
////        }
//        if (userLogList.size() > 0) {
//            System.out.println("series x: " + series1.getXaxis());
//            for (int i = 0; i < userLogList.size(); i++) {
//                series1.set(i, userLogList.get(i).getCount());
//                series1.set(i,userLogList.get(i).getHour());
////                      series1.set(i, 2);
////                System.out.println("x:" + userLogList.get(i).getHour());
////                System.out.println("y:" + userLogList.get(i).getCount());
//            }
//        }

//        series1.set(2, 1);
//        series1.set(3, 3);
//        series1.set(4, 6);
//        series1.set(5, 8);
       
        model.addSeries(series1);
//        }
        return model;
    }
    
    public void createAnimateModelForPeriod(){
     System.out.println("in createAnimatedModelsForPeriod!");
        animatedModelForPeriod = initLineModelForPeriod();
        if (animatedModelForPeriod == null) {
            System.out.println("animatedModelForPeriod null");
        }
        if (CalendarView.date1ForPeriod!=null && CalendarView.date2ForPeriod!=null){
            animatedModelForPeriod.setTitle("Статистика за период: с " + String.format("%tF",CalendarView.date1ForPeriod) + " " + "по " + String.format("%tF",CalendarView.date2ForPeriod)) ;
        
        } else {
            System.out.println("title empty");
        }
        animatedModelForPeriod.setAnimate(true);
        animatedModelForPeriod.setLegendPosition("ne");
         animatedModelForPeriod.getAxis(AxisType.Y).setLabel("Кол-во посещений");
          Axis yAxis = animatedModelForPeriod.getAxis(AxisType.Y);
          yAxis.setMin(0);
         DateAxis axis = new DateAxis("Даты посещений");
         axis.setTickFormat("%d.%m.%Y");
         axis.setMin(String.format("%tF",CalendarView.date1ForPeriod));
         axis.setMax(String.format("%tF",CalendarView.date2ForPeriod));
         animatedModelForPeriod.getAxes().put(AxisType.X, axis);
//          animatedModelForPeriod.setStacked(true);
         
        
//         animatedModelForPeriod.setShowPointLabels(true);//показывает значение на графике
//       Axis xAxis = animatedModelForPeriod.getAxis(AxisType.X);
//       
//       xAxis.setMin(CalendarView.date1ForPeriod);
//       xAxis.setMax(CalendarView.date2ForPeriod);
//        xAxis.setTickFormat("%b %#d, %y");
//       animatedModel1.getAxis(AxisType.X).setTickFormat("%tD");
//       xAxis.setMax(31); 
//      xAxis.setMax(String.valueOf(CalendarView.date2ForPeriod));
        
//        Axis yAxis = animatedModelForPeriod.getAxis(AxisType.Y);
//        yAxis.setMin(0);
        
//        animatedModelForPeriod.getAxes().put(AxisType.X, xAxis);
    
    }
     public LineChartModel initLineModelForPeriod()  {
      System.out.println("in initLineModelForPeriod");
         LineChartModel model = new LineChartModel();
         ArrayList<UserLog> userLogList = new ArrayList<>();
         userLogList.addAll(CalendarView.userLogListForPeriod);
         LineChartSeries series2 = new LineChartSeries();
         series2.setLabel("Визиты");
         if (userLogList != null) {
             if (userLogList.size() > 0) {

                 try {
                     for (int i = 0; i < userLogList.size(); i++) {
                         series2.set(userLogList.get(i).getDate(), userLogList.get(i).getCount());
                     }
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }
         }
         model.addSeries(series2);
         return model;
    }
     
     

    public LineChartModel getAnimatedModel1() {
        return animatedModel1;
    }

    public void setAnimatedModel1(LineChartModel animatedModel1) {
        this.animatedModel1 = animatedModel1;
    }

    public LineChartModel getAnimatedModelForPeriod() {
        return animatedModelForPeriod;
    }

    public void setAnimatedModelForPeriod(LineChartModel animatedModelForPeriod) {
        this.animatedModelForPeriod = animatedModelForPeriod;
    }
    
      
    public void cleanGraphic(){
        System.out.println("In cleanGraphic");
        try{
            if(animatedModel1!=null){
                animatedModel1.clear();
            }
           if(animatedModelForPeriod!=null){
               animatedModelForPeriod.clear();
               
               Log.info("I CLEAR PERIOD BRO");
           }
              
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
