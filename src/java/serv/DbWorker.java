package serv;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Calendar;

/**
 *
 * @author alesha
 */
public class DbWorker {

    static Logger Log = Logger.getLogger(DbWorker.class.getName());
    public static ArrayList<UserLog> userLogList = new ArrayList<>();

    /**
     * Функция для получения списка всех полей из таблицы worklog
     * @return список объектов worklog
     */
    public static ArrayList<WorkLog> workloglist() {
        ArrayList<WorkLog> worklogresult = new ArrayList<>();
        Connection con = null;
        String sql = "SELECT idwork, seansid, userid,datasize,stations FROM worklog;";
        Statement st = null;
        ResultSet rs = null;
        try {
            con = ConnecttionDb.connect();
            st = con.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                WorkLog wo = new WorkLog();
                wo.setIdwork(rs.getInt(1));
                wo.setSeansid(rs.getInt(2));
                wo.setUserid(rs.getInt(3));
                wo.setDatasize(rs.getLong(4));
                wo.setDatasizeMB(Calculation.convertToMegaBytes(wo.getDatasize()));

                worklogresult.add(wo);

            }
            Log.info("6.7");
            System.out.println("Длина = " + worklogresult.size());
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return worklogresult;
    }

    /**
     * Функция для получения количества посещений за выбранный период времени 
     * функция универсальна, может использоваться для расчета значений как за один час так и за период времени в днях
     * 
     * @param tst1 дата и время начала периода
     * @param tst2 дата и время конца периода
     * @return
     */
    public static int countSeansesBetweenTwoTimestamps(Timestamp tst1, Timestamp tst2) throws SQLException {
        Log.info("tst1 = " + tst1);
        Log.info("tst2 = " + tst2);
        int resultCount = 0;
//        ArrayList<UserLog> resultlist = new ArrayList<>();
        Connection con = null;
//        String sql = "SELECT seansid, userid, tstamp FROM userlog where tstamp between ? and ?;";
        String sql = "select count(*) from userlog where tstamp between ? and ?";
        PreparedStatement pst = null;
        ResultSet rs = null;
        try {
            Log.info("into try");
            con = ConnecttionDb.connect();
            pst = con.prepareStatement(sql);
            pst.setTimestamp(1, tst1);
            pst.setTimestamp(2, tst2);
            rs = pst.executeQuery();
            while (rs.next()) {
                UserLog u = new UserLog();
                u.setCount(rs.getInt(1));
                resultCount = rs.getInt(1);
            }           
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            if (con != null) {
                con.close();
            }
        }
        return resultCount;
    }

    public ArrayList<UserLog> getUserLogList() {
        return userLogList;
    }

    public void setUserLogList(ArrayList<UserLog> userLogList) {
        this.userLogList = userLogList;
    }
}
