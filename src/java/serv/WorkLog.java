/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

/**
 *
 * @author alesha
     */
public class WorkLog {
    private int idwork;
    private int seansid;
    private int userid;
    private long datasize;
    private String datasizeMB;
    private int[]stations;
    

    public int getIdwork() {
        return idwork;
    }

    public void setIdwork(int idwork) {
        this.idwork = idwork;
    }

    public int getSeansid() {
        return seansid;
    }

    public void setSeansid(int seansid) {
        this.seansid = seansid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public long getDatasize() {
        return datasize;
    }

//    public int getDatasize() {
//        return datasize;
//    }
//
//    public void setDatasize(int datasize) {
//        this.datasize = datasize;
    public void setDatasize(long datasize) {
        this.datasize = datasize;
    }

//    }
    public int[] getStations() {
        return stations;
    }

    public void setStations(int[] stations) {
        this.stations = stations;
    }

    public String getDatasizeMB() {
        return datasizeMB;
    }

    public void setDatasizeMB(String datasizeMB) {
        this.datasizeMB = datasizeMB;
    }

    
    
    
}
